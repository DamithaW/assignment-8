#include <stdio.h>
#include <stdlib.h>
struct student {
    char Name1[50];
    char sub;
    float marks;
}
s[10];

char main()
{
    char c;
    printf("Enter student information:\n");

    for (c = 0; c < 5; ++c)
        {
        s[c].sub = c + 1;
        printf("\nFor Subject %d,\n", s[c].sub);
        printf("Enter the first name: ");
        scanf("%s", s[c].Name1);
        printf("Enter the marks: ");
        scanf("%f", &s[c].marks);
        }
    printf("Displaying Information:\n\n");

    for (c = 0; c < 5; ++c)
        {
        printf("\nInstance: %d\n", c + 1);
        printf("First name: ");
        puts(s[c].Name1);
        printf("Marks: %.1f", s[c].marks);
        printf("\n");
        }
    return 0;
}